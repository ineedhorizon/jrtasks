package com.javarush.test.level07.lesson09.task05;

import java.io.*;
import java.util.ArrayList;

/* Удвой слова
1. Введи с клавиатуры 10 слов в список строк.
2. Метод doubleValues должен удваивать слова по принципу a,b,c -> a,a,b,b,c,c.
3. Используя цикл for выведи результат на экран, каждое значение с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<String> list = new ArrayList<String>();

        for (int i = 0; i < 10; i++) list.add(reader.readLine());


        ArrayList<String> result = doubleValues(list);

        for (String x : result) System.out.println(x);
    }

    public static ArrayList<String> doubleValues(ArrayList<String> list)
    {
        for (int i = 0; i < list.size(); i = i + 2) list.add(i, list.get(i));
        return list;
    }

//    public static ArrayList<String> doubleValues(ArrayList<String> list) Код, удваивает буквы во всех словах. Кто-то криво задания читает =(
//    {
//        for (int i = 0; i < list.size(); i++)
//            list.set(i, doubleWord(list.get(i)));
//
//        return list;
//    }

//    public static String doubleWord (String word)
//    {
//        char[] ch = word.toCharArray();
//        char[] doubleChar = new char[ch.length*2];
//
//        for (int i = 0; i < ch.length; i++)
//        {
//            doubleChar[i*2] = ch[i];
//            doubleChar[i*2 + 1] = ch[i];
//        }
//
//        return new String(doubleChar);
//    }
}
