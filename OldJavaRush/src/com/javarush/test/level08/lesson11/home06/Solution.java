
package com.javarush.test.level08.lesson11.home06;


/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/


import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<Human> kids = new ArrayList<Human>();
        kids.add(new Human("Yarik", true, 12, null));
        kids.add(new Human("Leshka", true, 5, null));
        kids.add(new Human("Lenka", false, 1, null));

        ArrayList<Human> parents = new ArrayList<Human>();
        parents.add(new Human("Sveta", false, 30, kids));
        parents.add(new Human("Kolya", false, 32, kids));

        ArrayList<Human> family = new ArrayList<Human>();
        family.add(new Human("Vasya", true, 45, parents));
        family.add(new Human("Petya", true, 56, parents));
        family.add(new Human("Fekla", false, 57, parents));
        family.add(new Human("Svekla", false, 50, parents));

        family.addAll(kids);
        family.addAll(parents);

        for (Human i : family)
        {
            System.out.println(i);
        }


    }

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children;


        private Human(String name, boolean sex, int age, ArrayList<Human> children)
        {
            this.age = age;
            this.name = name;
            this.sex = sex;
            this.children = children;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (children != null)
            {
                int childCount = this.children.size();
                if (childCount > 0)
                {
                    text += ", дети: " + this.children.get(0).name;

                    for (int i = 1; i < childCount; i++)
                    {
                        Human child = this.children.get(i);
                        text += ", " + child.name;
                    }
                }
            }

            return text;
        }
    }

}

