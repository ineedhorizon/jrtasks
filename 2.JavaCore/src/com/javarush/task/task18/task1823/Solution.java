package com.javarush.task.task18.task1823;

import com.sun.org.apache.xml.internal.utils.res.IntArrayWrapper;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* 
Нити и байты
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String fileName = reader.readLine();
            if (fileName.equals("exit")) {
                break;
            }
            new ReadThread(fileName).start();
        }
        reader.close();
    }

    public static class ReadThread extends Thread {
        private String fileName;

        public ReadThread(String fileName) throws FileNotFoundException {
            this.fileName = fileName;
        }

        private Map<Integer, Integer> map = new HashMap<>();

        public void run() {
            try {
                FileInputStream inputStream = new FileInputStream(fileName);
                while (inputStream.available() > 0) {
                    int data = inputStream.read();
                    if (map.containsKey(data)) {
                        map.merge(data, 1, Integer::sum);
                    } else {
                        map.put(data, 1);
                    }
                }

                int max = 0;
                int maxFrequency = 0;

                for (Map.Entry<Integer, Integer> pair: map.entrySet()) {
                    if(pair.getValue() > max){
                        max = pair.getValue();
                        maxFrequency = pair.getKey();
                    }
                }

                resultMap.put(fileName, maxFrequency);
                inputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}

