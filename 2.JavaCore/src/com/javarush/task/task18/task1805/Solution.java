package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* 
Сортировка байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        List<Integer> list = new ArrayList<>();
        List<Integer> copyList = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();

        FileInputStream inputStream = new FileInputStream(fileName);

        while (inputStream.available() > 0)
        {
            int data = inputStream.read();
            list.add(data);
        }
        reader.close();
        inputStream.close();

        for (Integer data: list) {
            if (!copyList.contains(data)){
                copyList.add(data);
            }
        }
        Collections.sort(copyList);

        for (Integer data: copyList){
            System.out.print(data + " ");
        }
    }
}
