package com.javarush.task.task18.task1825;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/* 
Собираем файл
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> list = new ArrayList<>();
        while (true) {
            String fileName = reader.readLine();
            if (fileName.equals("end")) {
                break;
            }
            list.add(fileName);
        }
        reader.close();
        Collections.sort(list);

        String outputFileName = list.get(0).split(".part")[0];
        FileOutputStream outputStream = new FileOutputStream(outputFileName);

        for (String aList : list) {
            FileInputStream inputStream = new FileInputStream(aList);
            byte[] buffer = new byte[inputStream.available()];
            inputStream.read(buffer);
            outputStream.write(buffer);
            inputStream.close();
        }
        outputStream.close();

    }
}
