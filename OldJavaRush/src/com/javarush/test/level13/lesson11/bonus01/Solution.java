package com.javarush.test.level13.lesson11.bonus01;

/* Сортировка четных чисел из файла
1. Ввести имя файла с консоли.
2. Прочитать из него набор чисел.
3. Вывести на консоль только четные, отсортированные по возрастанию.
Пример ввода:
5
8
11
3
2
10
Пример вывода:
2
8
10
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader keyReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader fileReader = new BufferedReader(new FileReader(keyReader.readLine()));
        keyReader.close();

        ArrayList<Integer> list = new ArrayList<Integer>();

        while (fileReader.ready())
        {
            Integer a = Integer.parseInt(fileReader.readLine());
            if (a % 2 == 0)
            {
                list.add(a);
            }
        }

        fileReader.close();
        Collections.sort(list);
        printList(list);
    }

    public static void printList(List<Integer> list)
    {
        for (Integer x : list) System.out.println(x);
    }
}
