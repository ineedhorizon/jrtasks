package com.javarush.task.task19.task1908;

/* 
Выделяем числа
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = bufferedReader.readLine();
        String fileName2 = bufferedReader.readLine();
        bufferedReader.close();

        String[] array = null;
                BufferedReader fileReader = new BufferedReader(new FileReader(fileName1));
        while (fileReader.ready()){
            array = fileReader.readLine().split(" ");
        }
        fileReader.close();

        StringBuilder stringBuilder = new StringBuilder();
        if (array != null) {
            for (String element: array) {
                try {
                    Integer i = Integer.valueOf(element);
                    stringBuilder = stringBuilder.append(i).append(" ");
                    System.out.println(i);
                } catch (NumberFormatException e){

                }
            }
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName2));
        System.out.println(stringBuilder.toString());
        writer.write(stringBuilder.toString());
        writer.close();

    }
}
