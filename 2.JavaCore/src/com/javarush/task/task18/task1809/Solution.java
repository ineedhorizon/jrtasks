package com.javarush.task.task18.task1809;

/* 
Реверс файла
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws IOException {
        List<Integer> list = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        FileInputStream inputStream = new FileInputStream(fileName1);

        while (inputStream.available() > 0) {
            int data = inputStream.read();
            list.add(data);
        }

        FileOutputStream outputStream = new FileOutputStream(fileName2);

        for (int i = list.size() - 1; i >= 0; i--) {
            outputStream.write(list.get(i));
        }

        outputStream.close();
        reader.close();
        inputStream.close();

    }
}
