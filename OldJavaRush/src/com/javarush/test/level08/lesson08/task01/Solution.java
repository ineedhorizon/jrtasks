package com.javarush.test.level08.lesson08.task01;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {
        HashSet<String> set = new HashSet<String>();
        set.add("лист");
        set.add("лиса");
        set.add("лестница");
        set.add("лес");
        set.add("листок");
        set.add("лодка");
        set.add("лоток");
        set.add("лупа");
        set.add("ласточка");
        set.add("лавка");
        set.add("ложка");
        set.add("луна");
        set.add("лошадь");
        set.add("лето");
        set.add("линейка");
        set.add("ложа");
        set.add("лень");
        set.add("лабуда");
        set.add("лак");
        set.add("лань");

        return set;
    }
}
