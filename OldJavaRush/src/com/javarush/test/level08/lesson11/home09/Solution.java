package com.javarush.test.level08.lesson11.home09;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;


/* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true, если количество дней с начала года -
нечетное число, иначе false
2. String date передается в формате MAY 1 2013
Не забудьте учесть первый день года.
Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false
*/

public class Solution
{
    public static void main(String[] args) throws IOException, ParseException
    {
        System.out.println(isDateOdd("JAN 2 2013"));
    }

    public static boolean isDateOdd(String date)
    {
/*        Date nowDate = new Date(date);
        Date nullDate = new Date(date);
        nullDate.setMonth(0);
        nullDate.setDate(1);

        long timeDistance = nowDate.getTime() - nullDate.getTime();

        int dayCount = (int)(timeDistance/(24*60*60*1000));*/

        Date nowDate = new Date(date);
        long finish = nowDate.getTime();
        nowDate.setMonth(0);
        nowDate.setDate(1);
        long start = nowDate.getTime();
        long msDay = 1000 * 60 *60 *24;
        int dayCount = (int)((finish-start) / msDay);

        if (dayCount%2 != 0) return false;
        else return true;
    }
}
