package com.javarush.task.task18.task1826;

/* 
Шифровка
*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {

        switch (args[0]) {
            case "-e":
                encrypt(args[1], args[2]);
                break;
            case "-d":
                decrypt(args[1], args[2]);
                break;
        }
    }

    public static void encrypt(String fileName, String fileOutputName) throws IOException {
        FileInputStream inputStream = new FileInputStream(fileName);
        FileOutputStream outputStream = new FileOutputStream(fileOutputName);
        while (inputStream.available() > 0){
            int data = inputStream.read() + 1;
            outputStream.write(data);
        }
        inputStream.close();
        outputStream.close();
    }

    public static void decrypt(String fileName, String fileOutputName) throws IOException {
        FileInputStream inputStream = new FileInputStream(fileName);
        FileOutputStream outputStream = new FileOutputStream(fileOutputName);
        while (inputStream.available() > 0){
            int data = inputStream.read() - 1;
            outputStream.write(data);
        }
        inputStream.close();
        outputStream.close();
    }
}
