package com.javarush.task.task18.task1804;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* 
Самые редкие байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Map<Integer, Integer> map = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();

        FileInputStream inputStream = new FileInputStream(fileName);

        while (inputStream.available() > 0)
        {
            int data = inputStream.read();
            if (map.containsKey(data)){
                map.merge(data, 1, Integer::sum);
            } else {
                map.put(data, 1);}
        }
        reader.close();
        inputStream.close();

        int min = 1;
        for (Map.Entry entry: map.entrySet()) {
            if ((int)entry.getValue() < min) {
                min = (int)entry.getValue();
            }
        }

        for (Map.Entry entry: map.entrySet()) {
            if ((int)entry.getValue() == min) {
                System.out.print(entry.getKey() + " ");
            }
        }
    }
}
