package com.javarush.task.task18.task1818;

/* 
Два в одном
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        String fileName3 = reader.readLine();

        FileOutputStream outputStream = new FileOutputStream(fileName1);
        FileInputStream inputStream1 = new FileInputStream(fileName2);
        FileInputStream inputStream2 = new FileInputStream(fileName3);

        while (inputStream1.available() > 0){
            int data = inputStream1.read();
            outputStream.write(data);
        }

        while (inputStream2.available() > 0){
            int data = inputStream2.read();
            outputStream.write(data);
        }

        inputStream1.close();
        inputStream2.close();
        outputStream.close();
    }
}
