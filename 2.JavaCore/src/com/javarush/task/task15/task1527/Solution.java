package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/* 
Парсер реквестов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        HashMap<String, String> map = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        URL url = new URL(reader.readLine());
        String query = url.getQuery();
        String[] rawParam = query.split("&");

        for (int i = 0; i < rawParam.length; i++){
            String[] params = rawParam[i].split("=");
            map.put(params[0], params.length == 2 ? params[1] : "");
            System.out.print(params[0] + " ");
        }

        for (Map.Entry<String, String> entry : map.entrySet()) {
            if (entry.getKey().equals("obj")){
                try {
                    System.out.println();
                    alert(Double.parseDouble(entry.getValue()));
                } catch (Exception e) {
                    System.out.println();
                    alert(entry.getValue());
                }
            }
        }
    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}
