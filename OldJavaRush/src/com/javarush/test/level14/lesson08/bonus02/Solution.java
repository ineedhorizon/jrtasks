package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int a = parseInt(reader.readLine());
        int b = parseInt(reader.readLine());

        System.out.println(nod(a, b));
        /*ArrayList<Integer> listA = new ArrayList<Integer>();
        ArrayList<Integer> listB = new ArrayList<Integer>();

        int del = 2;

        while(a != 1)
        {
            if (a%del == 0){
                listA.add(del);
                int e = a/del;
                a = e;
            }
            else if (a%del != 0)
            {
                del++;
            }
        }

        listA.add(1);

        int del2 = 2;

        while(b != 1)
        {
            if (b%del2 == 0){
                listB.add(del2);
                int e = b/del2;
                b = e;
            }
            else if (b%del2 != 0)
            {
                del2++;
            }
        }

        listB.add(1);

        for (int i = 0; i < listA.size(); i++)
        {
            System.out.println(listA.get(i));
        }
        System.out.println("-----------------------");

        for (int i = 0; i < listB.size(); i++)
        {
            System.out.println(listB.get(i));
        }*/





    }

    private static int nod(int a, int b)
    {
        if (b == 0) return a;
        else return nod(b, a % b);
    }
}

