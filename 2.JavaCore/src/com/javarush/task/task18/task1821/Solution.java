package com.javarush.task.task18.task1821;

/* 
Встречаемость символов
*/

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream inputStream = new FileInputStream(args[0]);
        Map<Character, Integer> map = new TreeMap<>();

        while (inputStream.available() > 0){
            char data = (char) inputStream.read();

            if (map.containsKey(data)){
                map.merge(data, 1, Integer::sum);
            } else {
                map.put(data, 1);
            }
        }

        for (Map.Entry<Character, Integer> pair: map.entrySet()) {
            System.out.println(pair.getKey() + " " + pair.getValue());
        }

        inputStream.close();
    }
}
