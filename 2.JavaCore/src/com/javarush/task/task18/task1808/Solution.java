package com.javarush.task.task18.task1808;

/* 
Разделение файла
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        String fileName3 = reader.readLine();

        FileInputStream inputStream = new FileInputStream(fileName1);
        FileOutputStream outputStream2 = new FileOutputStream(fileName2);
        FileOutputStream outputStream3 = new FileOutputStream(fileName3);

        boolean odd = inputStream.available() % 2 != 0;
        byte[] buffer = new byte[inputStream.available()];
        int count = 0;
        int offSet = 0;

        while (inputStream.available() > 0) {
            count = inputStream.read(buffer);
        }

        if (odd){
            outputStream2.write(buffer,0, count/2 + 1);
            offSet = count/2 + 1;
        } else {
            outputStream2.write(buffer, 0, count/2);
            offSet = count/2;
        }

        outputStream3.write(buffer, offSet, count/2);
        reader.close();
        inputStream.close();
        outputStream2.close();
        outputStream3.close();
    }
}
