package com.javarush.task.task18.task1817;

/* 
Пробелы
*/

import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream inputStream = new FileInputStream(args[0]);
        int countSymbol = 0;
        int countSpace = 0;
        while (inputStream.available() > 0) {
            int data = inputStream.read();
            if (' ' == data){
                countSpace++;
            } else {
                countSymbol++;
            }
        }

        System.out.printf("%.2f", ((float)countSpace)/(countSymbol + countSpace)*100.0);
        inputStream.close();
    }
}
