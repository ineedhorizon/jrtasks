package com.javarush.task.task17.task1711;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) {
        switch (args[0]) {
            case ("-c"):
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i+=3) {
                        createPerson(args[i + 1], args[i], parseDate(args[i + 2]));
                    }
                    return;
                }
            case ("-u"):
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i+=4) {
                        updatePerson(parseIndex(args[i]), args[i + 2], args[i + 1], parseDate(args[i + 3]));
                    }
                    return;
                }
            case ("-d"):
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        deletePerson(parseIndex(args[i]));
                    }
                    return;
                }
            case ("-i"):
                synchronized (allPeople) {
                    for (int i = 1; i < args.length; i++) {
                        infoPerson(parseIndex(args[i]));
                    }
                    return;
                }
        }
    }

    public static Date parseDate(String stringDate) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = dateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.getMessage();
        }
        return date;
    }

    public static int parseIndex(String stringIndex) {
        int index = Integer.parseInt(stringIndex);
        return index;
    }

    public static void createPerson(String sex, String name, Date date) {
        Person person = null;
        if (sex.equals("м")) {
            person = Person.createMale(name, date);
        } else {
            person = Person.createFemale(name, date);
        }
        allPeople.add(person);
        System.out.println(allPeople.indexOf(person));
    }

    public static void updatePerson(int index, String sex, String name, Date date) {
        allPeople.get(index).setName(name);
        if (sex.startsWith("м")) {
            allPeople.get(index).setSex(Sex.MALE);
        } else {
            allPeople.get(index).setSex(Sex.FEMALE);
        }
        allPeople.get(index).setBirthDate(date);
    }

    public static void deletePerson(int index) {
        allPeople.get(index).setName(null);
        allPeople.get(index).setSex(null);
        allPeople.get(index).setBirthDate(null);
    }

    public static void infoPerson(int index) {
        DateFormat dateFormatPrt = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        StringBuilder sb = new StringBuilder();
        sb.append(allPeople.get(index).getName()).append(" ")
                .append(allPeople.get(index).getSex() == Sex.MALE ? "м" : "ж")
                .append(" ")
                .append(dateFormatPrt.format(allPeople.get(index).getBirthDate()));
        System.out.println(sb);
    }

//    public static void debug() {
//        DateFormat dateFormatPrt = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
//        for (Person person : allPeople) {
//            StringBuffer s = new StringBuffer();
//            s.append(person.getName());
//            s.append(" ");
//            s.append(person.getSex() == Sex.MALE ? "м" : "ж");
//            s.append(" ");
//            if (person.getBirthDate()!=null)
//                s.append(dateFormatPrt.format(person.getBirthDate()));
//            System.out.println(s);
//        }
//    }

}
