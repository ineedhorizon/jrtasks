package com.javarush.test.level14.lesson08.home02;

/**
 * Created by kseniya.vorobyeva on 12/1/2016.
 */
public abstract class Drink
{
    public void taste()
    {
        System.out.println("Вкусно");
    }
}
