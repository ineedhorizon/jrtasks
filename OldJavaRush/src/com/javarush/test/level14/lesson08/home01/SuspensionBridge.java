package com.javarush.test.level14.lesson08.home01;

/**
 * Created by kseniya.vorobyeva on 12/1/2016.
 */
public class SuspensionBridge implements Bridge
{
    public int getCarsCount()
    {
        return 5;
    }
}
