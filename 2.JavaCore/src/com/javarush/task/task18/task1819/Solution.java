package com.javarush.task.task18.task1819;

/* 
Объединение файлов
*/

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();

        FileInputStream inputStream1 = new FileInputStream(fileName1);

        byte[] buffer = new byte[inputStream1.available()];
        inputStream1.read(buffer);

        FileInputStream inputStream2 = new FileInputStream(fileName2);
        FileOutputStream outputStream = new FileOutputStream(fileName1);

        while (inputStream2.available() > 0){
            int data = inputStream2.read();
            outputStream.write(data);
        }

        outputStream.write(buffer);

        inputStream1.close();
        inputStream2.close();
        outputStream.close();
    }
}
