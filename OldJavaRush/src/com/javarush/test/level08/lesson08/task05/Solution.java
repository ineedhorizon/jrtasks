package com.javarush.test.level08.lesson08.task05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
        public static void main(String[] args)
    {
        HashMap<String, String> map = createMap();
        removeTheFirstNameDuplicates(map);
        for (Map.Entry<String, String> pair : map.entrySet())
        {
            String key = pair.getKey();
            String value = pair.getValue();
            System.out.println(key + ":" + value);
        }
    }
    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Иванов", "Данил");
        map.put("Петров", "Алексей");
        map.put("Сидоров", "Коля");
        map.put("Тихонов", "Данил");
        map.put("Подберезная", "Галина");
        map.put("Исаев", "Иван");
        map.put("Самаковский", "Андрей");
        map.put("Рычков", "Алексей");
        map.put("Буков", "Александр");
        map.put("Малютенко", "Тимофей");

        return map;
    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {

        ArrayList<String> values = new ArrayList<String>(map.values());
        for(String value: values)
        {
            int count = 0;
            for (int i = 0; i < values.size(); i++)
            {
                if(value.equals(values.get(i)))
                {
                    count++;
                }

                if(count > 1)
                {
                    removeItemFromMapByValue(map, value);
                }
            }
        }


    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}
