package com.javarush.test.level06.lesson11.home07;

import com.sun.org.apache.xpath.internal.operations.String;


/* Три статические переменных name
Создай 3 public статических переменных: String Solution.name, String Cat.name, String Dog.name
*/

public class Solution
{

    public static String name;


    public static class Cat
    {
        public static String name;
    }

    public static class Dog
    {
        public static String name;
    }
}