package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human).
Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        Human humGranFa1 = new Human("Петя", true, 60);
        Human humGranFa2 = new Human("Вася", true, 65);
        Human humGranMa1 = new Human("Валя", false, 57);
        Human humGranMa2 = new Human("Галя", false, 61);
        Human humPa = new Human("Миша", true, 40);
        Human humMa = new Human("Катя", false, 36);
        Human humSon1 = new Human("Краб", true, 18);
        Human humSon2 = new Human("Рак", true, 17);
        Human humDaughter = new Human("Стримерша Карина", false, 15);

        System.out.println(humGranFa1.toString());
        System.out.println(humGranFa2.toString());
        System.out.println(humGranMa1.toString());
        System.out.println(humGranMa2.toString());
        System.out.println(humPa.toString());
        System.out.println(humMa.toString());
        System.out.println(humSon1.toString());
        System.out.println(humSon2.toString());
        System.out.println(humDaughter.toString());
    }

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        Human father, mother;

        public Human (String name, boolean sex, int age, Human father, Human mother)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public Human (String name, boolean sex, int age)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = null;
            this.mother = null;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}
