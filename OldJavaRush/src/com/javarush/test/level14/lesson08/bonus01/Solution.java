package com.javarush.test.level14.lesson08.bonus01;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* Нашествие эксепшенов
Заполни массив exceptions 10 различными эксепшенами.
Первое исключение уже реализовано в методе initExceptions.
*/

public class Solution
{
    public static List<Exception> exceptions = new ArrayList<Exception>();

    public static void main(String[] args)
    {
        initExceptions();

        for (Exception exception : exceptions)
        {
            System.out.println(exception);
        }
    }

    private static void initExceptions()
    {   //it's first exception
        try
        {
            float i = 1 / 0;

        } catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            int[] a = new int[10];
            int s = a[20];

        } catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            String s = null;
            s.getBytes();

        } catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            Integer.parseInt("privet");

        } catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            Object a[] = new String[5];
            a[0] = Integer.valueOf(100);

        } catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            Object i = Integer.valueOf(42);
            String s = (String)i;

        } catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            int[] a = new int[-5];

        } catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            InputStream is = new FileInputStream("111.txt");

        } catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            String s = "123";
            char ch = s.charAt(10);

        } catch (Exception e)
        {
            exceptions.add(e);
        }
        try
        {
            throw new CharConversionException();

        } catch (Exception e)
        {
            exceptions.add(e);
        }




        //Add your code here

    }

  }

