package com.javarush.task.task18.task1820;

/* 
Округление чисел
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = reader.readLine();
        String fileName2 = reader.readLine();
        reader.close();

        Scanner scanner = new Scanner(new FileInputStream(fileName1));
        scanner.useLocale(Locale.ENGLISH);
        ArrayList<Float> list = new ArrayList<Float>();
        while (scanner.hasNext()) {
            list.add(scanner.nextFloat());
        }
        scanner.close();

        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName2));
        for (float number: list) {
            writer.write(Math.round(number) + " ");
        }
        writer.close();
    }
}
