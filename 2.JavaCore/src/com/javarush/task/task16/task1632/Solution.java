package com.javarush.task.task16.task1632;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static List<Thread> threads = new ArrayList<>(5);

    static {
        threads.add(new ThreadEndless());
        threads.add(new ThreadInterrupted());
        threads.add(new ThreadUra());
        threads.add(new ThreadMessage());
        threads.add(new ThreadConsole());
    }
    public static void main(String[] args) {
        for (Thread thread: threads) {
            thread.start();
        }
    }

    public static class ThreadEndless extends Thread{
        public void run(){
            while (true){
            }
        }
    }
    public static class ThreadInterrupted extends Thread {
       public void run(){
           try {
               Thread.sleep(100000000);
           } catch (InterruptedException e) {
               System.out.println("InterruptedException");
           }
       }
    }

    public static class ThreadUra extends Thread {
        public void run() {
            while (true) {
                System.out.println("Ура");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class ThreadMessage extends Thread implements Message{

        private boolean die = false;

        @Override
        public void run() {
            while (die == false) {
            }
        }

        @Override
        public void showWarning() {
            die = true;
        }
    }

    public static class ThreadConsole extends Thread{
        public void run() {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int sum = 0;
            String s = null;
            while (true) {
                try {
                    s = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (s.equals("N") == true)
                    break;

                sum += Integer.parseInt(s);
            }
            System.out.println(sum);
        }
    }
}