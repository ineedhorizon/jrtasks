package com.javarush.test.level14.lesson08.home01;

/**
 * Created by kseniya.vorobyeva on 12/1/2016.
 */
public interface Bridge
{
    int getCarsCount();
}
