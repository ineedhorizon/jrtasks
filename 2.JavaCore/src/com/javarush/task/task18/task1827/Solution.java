package com.javarush.task.task18.task1827;

/* 
Прайсы
*/

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
    public static void main(String[] args) throws Exception {

        if (args.length < 4 || !args[0].equals("-c")) return;
        List<String> listArgs = appendProductName(Arrays.asList(args));
        Double price = Double.parseDouble(listArgs.get(2));
//        System.out.println(price);
        String productName = listArgs.get(1);
//        System.out.println(productName);
        Integer quantity = Integer.valueOf(listArgs.get(3));
//        System.out.println(quantity);

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();
//        String fileName = "1";

        BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
        String line;
        int maxId = 0;
        List<String> listText = new ArrayList<>();
        while ((line = fileReader.readLine()) != null) {

            listText.add(line);
            if (maxId < Integer.parseInt(line.substring(0, 8).trim())) {
                maxId = Integer.parseInt(line.substring(0, 8).trim());
            }
        }
        FileOutputStream outputStream = new FileOutputStream(fileName);
        for (String s : listText) {
            outputStream.write((String.format("%s%n", s)).getBytes());
        }

        outputStream.write(String.format("%-8s%-30s%-8s%-4s%n", ++maxId, productName, price, quantity).getBytes());
        outputStream.close();
        fileReader.close();
    }

    public static List<String> appendProductName(List<String> list){
        if (list.size() == 4){
            return list;
        } else {
            StringBuilder sb = new StringBuilder();
            sb = sb.append(list.get(1)).append(" ").append(list.get(2));
            list.set(1, sb.toString());
            list.remove(2);
            return appendProductName(list);
        }
    }
}
