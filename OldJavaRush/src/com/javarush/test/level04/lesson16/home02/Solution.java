package com.javarush.test.level04.lesson16.home02;

import java.io.*;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них. Т.е. не самое большое и не самое маленькое.
*/

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int a = Integer.parseInt(reader.readLine());
        int b = Integer.parseInt(reader.readLine());
        int c = Integer.parseInt(reader.readLine());

        System.out.println(mid(a,b,c));
    }

    public static int mid(int a, int b, int c){
        if(a>b && a<c || a>c && a<b) return a;
        if(b>a && b<c || a>b && b>c) return b;
        if(c<b && c>a || c>b && c<a) return c;
        return 0;
    }
}
