package com.javarush.task.task19.task1904;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/* 
И еще один адаптер
*/

public class Solution {

    public static void main(String[] args) {

    }

    public static class PersonScannerAdapter implements PersonScanner {
        private Scanner fileScanner;

        public PersonScannerAdapter(Scanner scanner){
            this.fileScanner = scanner;
        }

        @Override
        public Person read() throws IOException {
            String line = this.fileScanner.nextLine();
            String[] parts = line.split(" ");

            String firstName = parts[1];
            String middleName = parts[2];
            String lastName = parts[0];

            Date birthDate = null;
            StringBuilder sb = new StringBuilder();
            sb = sb.append(parts[3]).append(".").append(parts[4]).append(".").append(parts[5]);
            SimpleDateFormat format = new SimpleDateFormat();
            format.applyPattern("dd.MM.yyyy");
            try {
                birthDate= format.parse(sb.toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return new Person(firstName, middleName, lastName, birthDate);
        }

        @Override
        public void close() throws IOException {
            this.fileScanner.close();
        }
    }
}
