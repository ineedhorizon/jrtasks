package com.javarush.task.task18.task1803;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* 
Самые частые байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Map<Integer, Integer> map = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();

        FileInputStream inputStream = new FileInputStream(fileName);

        while (inputStream.available() > 0)
        {
            int data = inputStream.read();
            if (map.containsKey(data)){
//                System.out.println(map.get(data));
                map.merge(data, 1, Integer::sum);
//                System.out.println(map.get(data) + 1234567);
            } else {
                map.put(data, 1);}
        }
        reader.close();
        inputStream.close();

        int max = 0;
        for (Map.Entry entry: map.entrySet()) {
//            System.out.println(entry.getValue());
            if ((int)entry.getValue() > max) {
                max = (int)entry.getValue();
            }
        }

        for (Map.Entry entry: map.entrySet()) {
            if ((int)entry.getValue() == max) {
                System.out.print(entry.getKey() + " ");
            }
        }
    }
}
