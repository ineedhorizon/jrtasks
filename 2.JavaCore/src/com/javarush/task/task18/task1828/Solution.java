package com.javarush.task.task18.task1828;

/* 
Прайсы 2
*/

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
    //    public static void main(String[] args) throws IOException {
//        String id;
//        String line;
//        List<String> list = new ArrayList<>();
//
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        String fileName = reader.readLine();
////        String fileName = "1";
//        reader.close();
//
//        args = new String[]{"-d", "2"};
//
//        if (args.length < 2) {
//            return;
//        }
//
//        switch (args[0]) {
//            case "-u":
//                List<String> listArgs = appendProductName(Arrays.asList(args));
//                id = listArgs.get(1);
//                String productName = listArgs.get(2);
//                Double price = Double.parseDouble(listArgs.get(3));
//                Integer quantity = Integer.valueOf(listArgs.get(4));
//
//                BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
//                line = null;
//
//                while ((line = fileReader.readLine()) != null) {
//                    if (line.substring(0, 8).trim().equals(id)) {
//                        if ("-u".equals(listArgs.get(0))) {
//                            list.add(String.format("%-8s%-30s%-8s%-4s", id, productName, price, quantity));
//                        }
//                    } else {
//                        list.add(line);
//                    }
//                }
//                FileOutputStream outputStream = new FileOutputStream(fileName);
//                for (String s : list) {
//                    outputStream.write((String.format("%s%n", s)).getBytes());
//                }
//                outputStream.close();
//                fileReader.close();
//                break;
//            case "-d":
//                id = args[1];
//
//                BufferedReader fileReader2 = new BufferedReader(new FileReader(fileName));
//                line = null;
//
//                while ((line = fileReader2.readLine()) != null) {
//                    if (!line.substring(0, 8).trim().equals(id)) {
//                        list.add(line);
//                    }
//                }
//                FileOutputStream outputStream2 = new FileOutputStream(fileName);
//                for (String s : list) {
//                    outputStream2.write((String.format("%s%n", s)).getBytes());
//                }
//                outputStream2.close();
//                fileReader2.close();
//                break;
//            default:
//                return;
//        }
//    }
//

    public static void main(String[] args) throws IOException {
//        args = new String[]{"-u", "3", "123456789012345678901234567890", "12345678", "1234"};
//      args = new String[]{"-d", "5"};

        if (args.length < 2 || !(args[0].equals("-u") || args[0].equals("-d"))) return;

//        String fileName = "1";
        String id = args[1];

        //считываем имя файла, закрываем поток
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = reader.readLine();
        reader.close();

        List<String> list = new ArrayList<>();
        BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
        String line = null;

        if (args[0].equals("-u")) {
            List<String> listArgs = appendProductName(Arrays.asList(args));
            String productName = listArgs.get(2);
            Double price = Double.valueOf(listArgs.get(3));
            Integer quantity = Integer.valueOf(listArgs.get(4));
            while ((line = fileReader.readLine()) != null) {
                if (line.substring(0, 8).trim().equals(id)) {
                    if ("-u".equals(listArgs.get(0))) {
                        list.add(String.format("%-8s%-30s%-8.2f%-4s", id, productName, price, quantity));
                    }
                } else {
                    list.add(line);
                }
            }
        } else if (args[0].equals("-d")) {
            while ((line = fileReader.readLine()) != null) {
                if (!line.substring(0, 8).trim().equals(id)) {
                    list.add(line);
                }
            }
        } else {
            return;
        }

        FileOutputStream outputStream = new FileOutputStream(fileName);
        for (String s : list) {
            outputStream.write((String.format("%s%n", s)).getBytes());
        }
        outputStream.close();
        fileReader.close();
    }

    public static List<String> appendProductName(List<String> list) {
        if (list.size() == 5) {
            return list;
        } else {
            StringBuilder sb = new StringBuilder();
            sb = sb.append(list.get(2)).append(" ").append(list.get(3));
            list.set(1, sb.toString());
            list.remove(3);
            return appendProductName(list);
        }
    }
}
