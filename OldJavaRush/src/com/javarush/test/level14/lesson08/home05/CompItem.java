package com.javarush.test.level14.lesson08.home05;

/**
 * Created by kseniya.vorobyeva on 12/8/2016.
 */
public interface CompItem
{
    String getName();
}
